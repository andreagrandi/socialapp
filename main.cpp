#include <QtGui/QApplication>
#include <QNetworkProxy>
#include "qmlapplicationviewer.h"

// socialconnect additions begin

#include "socialconnectplugin.h"

// socialconnect additions end

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    // socialconnect additions begin

    SocialConnectPlugin plugin;
    plugin.registerTypes("SocialConnect");

    // socialconnect additions end

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);

    //viewer.setMainQmlFile(QLatin1String("qml/socialapp/facebook.qml"));
    //viewer.setMainQmlFile(QLatin1String("qml/socialapp/instagram.qml"));
    viewer.setMainQmlFile(QLatin1String("qml/socialapp/twitter.qml"));

    viewer.showExpanded();

    /*

    // just in case the provided Internet connection must use a proxy

    QNetworkProxy proxy;
    proxy.setType(QNetworkProxy::HttpProxy);
    proxy.setHostName("proxy.example.com");
    proxy.setPort(1080);
    QNetworkProxy::setApplicationProxy(proxy);

    */

    return app->exec();
}
