// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import QtWebKit 1.0
import SocialConnect 1.0

Rectangle {
    Component.onCompleted: facebookConnection.authenticate();

    // engine declaration begins

    Timer {
        id: timer
        interval: 1000
        onTriggered: facebookConnection.retrieveLatest();
    }

    ListModel {
        id: listModel
    }

    WebInterface {
        id: webInterface
        onUrlChanged: loader.item.url = url;
    }

    FacebookConnection {
        id: facebookConnection
        webInterface: webInterface
        permissions: ["publish_stream", "read_stream", "friends_status"]
        clientId: "168329996633533" // TODO add facebook application id here
        onAuthenticateCompleted: success ? retrieveLatest() : authenticate();
        onPostMessageCompleted: success ? timer.start() : authenticate();

        onRetrieveMessagesCompleted: {
            listModel.clear();

            for (var i = 0; i < messages.length; i++) {
                listModel.append(messages[i]);
            }
        }

        function postText(text) {
            postMessage({ text: text });
        }

        function retrieveLatest() {
            facebookConnection.retrieveMessages(0, Math.round((new Date()).getTime() / 1000), 64);
        }
    }

    // engine declaration ends

    // ui declaration begins

    Loader {
        id: loader
        anchors.fill: parent
        sourceComponent: facebookConnection.authenticated ?
                             postingComponent : authenticationComponent
    }

    Component {
        id: authenticationComponent

        WebView {
            preferredHeight: parent.height
            preferredWidth: parent.width
            onUrlChanged: webInterface.url = url;
        }
    }

    Component {
        id: postingComponent

        Column {
            anchors.fill: parent

            TextInput {
                id: textInput
                width: parent.width
                height: implicitHeight
                horizontalAlignment: TextInput.AlignHCenter
                focus: true
            }

            Rectangle {
                id: button
                width: parent.width
                height: text.height * 2
                color: mouseArea.pressed ? "#335190" : "#3B5998"

                Text {
                    id: text
                    height: implicitHeight
                    anchors.centerIn: parent
                    text: "Post to Facebook as " + facebookConnection.name
                    color: textInput.text.length > 0 ? "white" : "gray"
                }

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    enabled: textInput.text.length > 0

                    onClicked: {
                        facebookConnection.postText(textInput.text);
                        textInput.text = "";
                    }
                }
            }

            ListView {
                width: parent.width
                height: parent.height - textInput.height - button.height
                model: listModel
                clip: true

                delegate: Text {
                    width: parent.width
                    height: implicitHeight
                    text: model.text
                }
            }
        }
    }

    // ui declaration ends
}
