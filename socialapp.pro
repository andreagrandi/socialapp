folder_01.source = qml/socialapp
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xE75A7ADC
symbian:TARGET.CAPABILITY += NetworkServices

SOURCES += main.cpp

# socialconnect additions begin

QT += declarative network script

INCLUDEPATH += socialconnect

HEADERS += \
    socialconnect/socialconnectplugin.h \
    socialconnect/socialconnection.h \
    socialconnect/socialconnectionerror.h \
    socialconnect/webinterface.h \
    socialconnect/facebook/facebookconnection.h \
    socialconnect/facebook/facebook.h \
    socialconnect/facebook/facebookrequest.h \
    socialconnect/facebook/facebookreply.h \
    socialconnect/facebook/facebookdatamanager.h \
    socialconnect/twitter/twitterconnection.h \
    socialconnect/twitter/twitterrequest.h \
    socialconnect/twitter/twitterconstants.h \
    socialconnect/instagram/instagramconnection.h \
    socialconnect/instagram/instagramrequest.h \
    socialconnect/instagram/instagramconstants.h

SOURCES += \
    socialconnect/socialconnectplugin.cpp \
    socialconnect/socialconnection.cpp \
    socialconnect/socialconnectionerror.cpp \
    socialconnect/webinterface.cpp \
    socialconnect/facebook/facebookconnection.cpp \
    socialconnect/facebook/facebook.cpp \
    socialconnect/facebook/facebookrequest.cpp \
    socialconnect/facebook/facebookreply.cpp \
    socialconnect/facebook/facebookdatamanager.cpp \
    socialconnect/twitter/twitterconnection.cpp \
    socialconnect/twitter/twitterrequest.cpp \
    socialconnect/instagram/instagramconnection.cpp \
    socialconnect/instagram/instagramrequest.cpp

symbian:TARGET.CAPABILITY += ReadUserData

# socialconnect additions end

include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()
